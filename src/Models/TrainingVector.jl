
module TrainingVector

export TrainingVectorStruct

mutable struct TrainingVectorStruct
    values::Vector{Cfloat}
    class::Cint
end

function create(length::Cint, class::Cint)
    TrainingVectorStruct([Cfloat(0) for i in 1:length], class)
end

function create(values::Vector{Cfloat}, class::Cint)
    TrainingVectorStruct(values, class)
end

function resize(vector::TrainingVectorStruct, size::Cint)
    while size < length(vector.values)
        pop!(vector.values)
    end
    while size > length(vector.values)
        push!(vector.values, Cfloat(0))
    end
end

end
