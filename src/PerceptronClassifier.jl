module PerceptronClassifier

using CImGui
using CImGui.GLFWBackend
using CImGui.OpenGLBackend
using CImGui.GLFWBackend.GLFW
using CImGui.OpenGLBackend.ModernGL

include("UI/UI.jl")
using .UI

const GLSL_VERSION = 410
const GLFW_WINDOW_HINTS = Dict(
    GLFW.CONTEXT_VERSION_MAJOR => 4,
    GLFW.CONTEXT_VERSION_MINOR => 1,
    GLFW.OPENGL_PROFILE => GLFW.OPENGL_CORE_PROFILE,
    GLFW.OPENGL_FORWARD_COMPAT => GL_TRUE,
)

function run()
    configure_glfw()
    window = MainWindow.create_window()

    MainWindow.run_loop(window)

    MainWindow.destroy_window(window)
end

function configure_glfw()
    for (hint_key, hint_value) in GLFW_WINDOW_HINTS
        GLFW.WindowHint(hint_key, hint_value)
    end

    GLFW.SetErrorCallback(error_handler)
end

function error_handler(error::GLFW.GLFWError)
    @error "GLFW ERROR: code $(error.code) message $(error.description)"
end

end

PerceptronClassifier.run()
