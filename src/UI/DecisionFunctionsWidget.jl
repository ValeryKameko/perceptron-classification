module DecisionFunctionsWidget

using Distributions
using Random
using Printf: @sprintf
using CImGui
using CImGui: ImVec2
using CImGui.CSyntax
using CImGui.CSyntax: pointer
using CImGui.CSyntax.CStatic
using CImGui.CSyntax.CRef
using CImGui.GLFWBackend
using CImGui.OpenGLBackend
using CImGui.GLFWBackend.GLFW
using CImGui.OpenGLBackend.ModernGL

include("../Models/Models.jl")
using .Models.TrainingVector

using ..MainWindow.InputWidget

export DecisionFunctionsWidgetStruct

MAX_INTERATIONS_COUNT = 1000
COEFFICIENT = 0.2
EPSILON = 0.001

mutable struct DecisionFunctionsWidgetStruct
    features_count
    classes_count
    functions_weights
end

function create()
    DecisionFunctionsWidgetStruct(1, 1, [0.0 1.0;])
end

function draw_ui(widget_struct::DecisionFunctionsWidgetStruct, input_widget::InputWidgetStruct)
    CImGui.Begin("Functions")

    if CImGui.Button("Train")
        train(widget_struct, input_widget.features_count, input_widget.classes_count, input_widget.vectors)
    end

    for i in 1:(widget_struct.classes_count)
        CImGui.SetNextItemWidth(30); CImGui.Text("d$(i)(x) = ")
        for j in 1:(widget_struct.features_count + 1)
            if j != 1
                CImGui.SameLine(); CImGui.Text(" + ")
            end
            CImGui.SameLine(); CImGui.SetNextItemWidth(100); CImGui.Text(@sprintf("%.2f", widget_struct.functions_weights[i, j]))
            if j != (widget_struct.features_count + 1)
                CImGui.SameLine(); CImGui.SetNextItemWidth(30); CImGui.Text(" * x$(j)")
            end
        end
    end

    CImGui.End()
end

function train(widget_struct::DecisionFunctionsWidgetStruct, features_count, classes_count, training_vectors)
    functions_weights = zeros(Float64, (classes_count, features_count + 1))
    print(functions_weights)
    error = false
    for iteration in 1:MAX_INTERATIONS_COUNT
        println(""); println("")
        println("*******************************************************")
        println("Iteration #$(iteration)")
        println("functions_weights = $(functions_weights)")

        error = false
        for training_vector in training_vectors
            vector = vcat([Float32(training_vector.values[i]) for i in 1:length(training_vector.values)], [Float32(1.0)])
            functions_results = calc_functions_results(functions_weights, vector)

            max_value = maximum(functions_results)
            max_indices = [i for (i, x) in enumerate(functions_results) if x >= max_value - EPSILON]
            max_index = max_indices[1]

            println("")
            println("vector = $(vector)         class = $(training_vector.class)")
            println("functions_results = $(functions_results)")
            println("max_index = $(max_index) != $(training_vector.class)")
            println("max_indices = $(max_indices)")
            if (max_index != training_vector.class) || (length(max_indices) != 1)
                error = true
                println("max_index = $(max_index)  !=  training_vector.class = $(training_vector.class)")
                for i in 1:(widget_struct.classes_count)
                    # print(functions_weights[i, :], " --> ", COEFFICIENT * vector)
                    if i == training_vector.class
                        functions_weights[i, :] += COEFFICIENT * vector
                        println("+patch[$(i)]  +=  $(COEFFICIENT * vector)")
                    elseif functions_results[i] >= functions_results[training_vector.class]
                        functions_weights[i, :] -= COEFFICIENT * vector
                        println("+patch[$(i)]  -=  $(COEFFICIENT * vector)")
                    end
                end
            end

            if !error
                break
            end
        end
    end

    if error
        CImGui.OpenPopup("Error Popup")
    else
        println("Done111111")
        widget_struct.features_count = features_count
        widget_struct.classes_count = classes_count
        widget_struct.functions_weights = functions_weights
    end
    if CImGui.BeginPopupModal("Enter Popup")
        CImGui.Text("Algorithm is not converge")
        CImGui.EndPopup()
    end
end

function calc_functions_results(functions_weights, vector)
    functions_weights * vector
end

end
