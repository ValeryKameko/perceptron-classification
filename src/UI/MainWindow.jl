module MainWindow

include("InputWidget.jl")
using .InputWidget

include("DecisionFunctionsWidget.jl")
using .DecisionFunctionsWidget

using Printf
using Colors
using CImGui
using CImGui.GLFWBackend
using CImGui.OpenGLBackend
using CImGui.GLFWBackend.GLFW
using CImGui.OpenGLBackend.ModernGL



using ....PerceptronClassifier

mutable struct MainWindowStruct
    glfw_window
    imgui_context
    input_widget::InputWidgetStruct
    decision_functions_widget::DecisionFunctionsWidgetStruct
end

function create_window()
    glfw_window = GLFW.CreateWindow(640, 800, "Perceptron Classifier")

    GLFW.MakeContextCurrent(glfw_window)
    GLFW.SwapInterval(1)

    imgui_context = CImGui.CreateContext()
    CImGui.StyleColorsDark()
    ImGui_ImplGlfw_InitForOpenGL(glfw_window, true)
    ImGui_ImplOpenGL3_Init(PerceptronClassifier.GLSL_VERSION)

    input_widget = InputWidget.create()
    decision_functions_widget = DecisionFunctionsWidget.create()
    MainWindowStruct(glfw_window, imgui_context, input_widget, decision_functions_widget)
end

function destroy_window(window)
    ImGui_ImplOpenGL3_Shutdown()
    ImGui_ImplGlfw_Shutdown()
    CImGui.DestroyContext(window.imgui_context)
    GLFW.DestroyWindow(window.glfw_window)
end

function run_loop(window)
    while true
        GLFW.MakeContextCurrent(window.glfw_window)
        CImGui.SetCurrentContext(window.imgui_context)
        GLFW.PollEvents()

        if GLFW.WindowShouldClose(window.glfw_window)
            break
        end

        ImGui_ImplOpenGL3_NewFrame()
        ImGui_ImplGlfw_NewFrame()
        CImGui.NewFrame()

        draw_ui(window)

        draw_imgui(window)
        GLFW.SwapBuffers(window.glfw_window)
    end
end

function draw_imgui(window)
    CImGui.Render()

    width, height = GLFW.GetFramebufferSize(window.glfw_window)
    glViewport(0, 0, width, height)

    clear_color = colorant"#2E3436"
    glClearColor(clear_color.r, clear_color.g, clear_color.b, 1.0)
    glClear(GL_COLOR_BUFFER_BIT)

    ImGui_ImplOpenGL3_RenderDrawData(CImGui.GetDrawData())
end

function draw_ui(window)
    InputWidget.draw_ui(window.input_widget)
    DecisionFunctionsWidget.draw_ui(window.decision_functions_widget, window.input_widget)
end

end
