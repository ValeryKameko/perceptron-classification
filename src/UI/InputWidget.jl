module InputWidget

using Distributions
using Random
using CImGui
using CImGui: ImVec2
using CImGui.CSyntax
using CImGui.CSyntax: pointer
using CImGui.CSyntax.CStatic
using CImGui.CSyntax.CRef
using CImGui.GLFWBackend
using CImGui.OpenGLBackend
using CImGui.GLFWBackend.GLFW
using CImGui.OpenGLBackend.ModernGL

include("../Models/Models.jl")
using .Models.TrainingVector

export InputWidgetStruct

mutable struct InputWidgetStruct
    vectors_count::Cint
    classes_count::Cint
    features_count::Cint
    vectors::Vector{TrainingVectorStruct}
end

function create()
    vectors_count = Cint(1)
    classes_count = Cint(1)
    features_count = Cint(1)
    input_widget = InputWidgetStruct(vectors_count, classes_count, features_count, [TrainingVector.create(features_count, Cint(1))])
    adjust_vectors(input_widget)
    input_widget
end

function draw_ui(input_widget::InputWidgetStruct)
    CImGui.Begin("Input")

    CImGui.Text("Vectors count (N): "); CImGui.SameLine()
    CImGui.SetNextItemWidth(100)
    @c CImGui.InputInt("##vectors_count", &(input_widget.vectors_count))
    adjust_vectors_count(input_widget)

    CImGui.Spacing();

    CImGui.Text("Features count (K): "); CImGui.SameLine()
    CImGui.SetNextItemWidth(100)
    @c CImGui.InputInt("##features_count", &(input_widget.features_count))
    adjust_features_count(input_widget)

    CImGui.Spacing();

    CImGui.Text("Classes count (M): "); CImGui.SameLine()
    CImGui.SetNextItemWidth(100)
    @c CImGui.InputInt("##classes_count", &(input_widget.classes_count))
    adjust_classes_count(input_widget)

    CImGui.Spacing();

    adjust_vectors(input_widget)

    if CImGui.Button("Randomize")
        randomize(input_widget)
    end

    CImGui.BeginChild("Vectors", ImVec2(0, 0), false, CImGui.ImGuiWindowFlags_HorizontalScrollbar)
    for i in 1:(input_widget.vectors_count)
        CImGui.PushID(i)
        for j in 1:(input_widget.features_count)
            CImGui.SetNextItemWidth(60)
            CImGui.PushID(j)
            @c CImGui.InputFloat("##input_matrix", &(input_widget.vectors[i].values[j - 1]), 0.0, 0.0, "%.3f")
            CImGui.SameLine()
            CImGui.PopID()
        end

        CImGui.Text("Class"); CImGui.SameLine()
        CImGui.SetNextItemWidth(60)
        @c CImGui.InputInt("##input_matrix_class", &(input_widget.vectors[i].class))
        adjust_class(input_widget, input_widget.vectors[i])
        CImGui.NewLine()

        CImGui.PopID()
    end
    CImGui.EndChild()

    CImGui.End()
end

function adjust_vectors(input_widget)
    while input_widget.vectors_count < length(input_widget.vectors)
        pop!(input_widget.vectors)
    end

    while input_widget.vectors_count > length(input_widget.vectors)
        vector = TrainingVector.create(input_widget.features_count, Cint(1))
        push!(input_widget.vectors, vector)
    end

    for i in 1:input_widget.vectors_count
        TrainingVector.resize(input_widget.vectors[i], input_widget.features_count)
        adjust_class(input_widget, input_widget.vectors[i])
    end
end

function adjust_class(input_widget, vector)
    if vector.class < 1
        vector.class = Cint(1)
    end

    if vector.class > input_widget.classes_count
        vector.class = input_widget.classes_count
    end
end

function adjust_features_count(input_widget)
    features_count_min = 1
    features_count_max = 50

    if input_widget.features_count < features_count_min
        input_widget.features_count = Cint(features_count_min)
    end

    if input_widget.features_count > features_count_max
        input_widget.features_count = Cint(features_count_max)
    end
end

function adjust_vectors_count(input_widget)
    vectors_count_min = 1
    vectors_count_max = 50

    if input_widget.vectors_count < vectors_count_min
        input_widget.vectors_count = Cint(vectors_count_min)
    end

    if input_widget.vectors_count > vectors_count_max
        input_widget.vectors_count = Cint(vectors_count_max)
    end
end

function adjust_classes_count(input_widget)
    classes_count_min = 1
    classes_count_max = min(50, input_widget.vectors_count)

    if input_widget.classes_count < classes_count_min
        input_widget.classes_count = Cint(classes_count_min)
    end

    if input_widget.classes_count > classes_count_max
        input_widget.classes_count = Cint(classes_count_max)
    end
end

function randomize(input_widget)
    for i in 1:(input_widget.vectors_count)
        for j in 1:(input_widget.features_count)
            input_widget.vectors[i].values[j] = Cfloat(rand(Uniform(-100, 100)))
        end
    end

    indices = [i for i in 1:input_widget.vectors_count]
    shuffle!(indices)

    for i in 1:(input_widget.classes_count)
        index = pop!(indices)
        input_widget.vectors[index].class = i
    end

    while length(indices) > 0
        index = pop!(indices)
        input_widget.vectors[index].class = rand(1:(input_widget.vectors_count))
    end
end

end
